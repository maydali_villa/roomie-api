package mx.roomie.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.roomie.model.HouseHold;
import mx.roomie.request.HouseHoldRequest;
import mx.roomie.service.HouseHoldService;

@RestController
public class HouseHoldResource {

    @Autowired
    private HouseHoldService houseHoldService;

    @PostMapping("/household")
    public ResponseEntity<HouseHold> saveHouseHold(@RequestBody HouseHoldRequest request) {
        HouseHold household = new HouseHold();
        household.setAddress_line1(request.getAddress_line1());
        household.setAddress_line2(request.getAddress_line2());
        household.setCity(request.getCity());
        household.setColony(request.getColony());
        household.setCountry(request.getCountry());
        household.setStreet(request.getStreet());
        // household.setUsers(request.getUsers());
        // household.setRules(request.getRules());
        // household.setImages(request.getImages());

        houseHoldService.saveHouseHold(household);

        return ResponseEntity.status(HttpStatus.CREATED).body(household);
    }

    @GetMapping("/households")
    public ResponseEntity<List<HouseHold>> getHouseHolds() {
        List<HouseHold> households = houseHoldService.getHouseHolds();
        ResponseEntity<List<HouseHold>> response = ResponseEntity.ok().body(households);
        return response;
    }

    @GetMapping("/household")
    public ResponseEntity<HouseHold> getHouseHold(@RequestParam("id") Integer id) {
        Optional<HouseHold> houseHoldOptional = houseHoldService.getHouseHoldById(id);

        if (!houseHoldOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        HouseHold houseHold = houseHoldOptional.get();
        return ResponseEntity.ok().body(houseHold);

    }

    @DeleteMapping("/household")
    public ResponseEntity<HouseHold> deleteHouseHold(@RequestParam("id") Integer id) {
        houseHoldService.deleteHouseHold(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PutMapping("/household")
    public ResponseEntity<HouseHold> updateHouseHold(@RequestParam("id") Integer id,
            @RequestBody HouseHoldRequest request) {
        Optional<HouseHold> houseHoldOptional = houseHoldService.getHouseHoldById(id);

        if (!houseHoldOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        HouseHold household = new HouseHold();
        household.setId(id);
        household.setAddress_line1(request.getAddress_line1() != null ? request.getAddress_line1()
                : houseHoldOptional.get().getAddress_line1());
        household.setAddress_line2(request.getAddress_line2() != null ? request.getAddress_line2()
                : houseHoldOptional.get().getAddress_line2());
        household.setCity(request.getCity() != null ? request.getCity() : houseHoldOptional.get().getCity());
        household.setColony(request.getColony() != null ? request.getColony() : houseHoldOptional.get().getColony());
        household
                .setCountry(request.getCountry() != null ? request.getCountry() : houseHoldOptional.get().getCountry());
        household.setStreet(request.getStreet() != null ? request.getStreet() : houseHoldOptional.get().getStreet());
        // household.setUsers(request.getUsers() != null ? request.getUsers() :
        // houseHoldOptional.get().getUsers());
        // household.setImages(request.getImages() != null ? request.getImages() :
        // houseHoldOptional.get().getImages());
        // household.setRules(request.getRules() != null ? request.getRules() :
        // houseHoldOptional.get().getRules());

        houseHoldService.saveHouseHold(household);

        return ResponseEntity.ok().body(household);
    }

}