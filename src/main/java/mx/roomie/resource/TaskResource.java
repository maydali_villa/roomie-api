package mx.roomie.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.roomie.model.Task;
import mx.roomie.request.TaskRequest;
import mx.roomie.service.TaskService;

@RestController
public class TaskResource {

    @Autowired
    private TaskService taskService;

    @PostMapping("/task")
    public ResponseEntity<Task> saveTask(@RequestBody TaskRequest request) {
        Task task = new Task();
        task.setTitle(request.getTitle());
        task.setDescription(request.getDescription());
        task.setUserId(request.getUserId());

        taskService.saveTask(task);

        return ResponseEntity.status(HttpStatus.CREATED).body(task);
    }

    @GetMapping("/tasks")
    public ResponseEntity<List<Task>> getTasks() {
        List<Task> tasks = taskService.getTasks();
        ResponseEntity<List<Task>> response = ResponseEntity.ok().body(tasks);
        return response;
    }

    @GetMapping("/task")
    public ResponseEntity<Task> getTask(@RequestParam("id") Integer id) {
        Optional<Task> taskOptional = taskService.getTaskById(id);

        if (!taskOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        Task task = taskOptional.get();
        return ResponseEntity.ok().body(task);

    }

    @DeleteMapping("/task")
    public ResponseEntity<Task> deleteTask(@RequestParam("id") Integer id) {
        taskService.deleteTask(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PutMapping("/task")
    public ResponseEntity<Task> updateTask(@RequestParam("id") Integer id, @RequestBody TaskRequest request) {
        Optional<Task> taskOptional = taskService.getTaskById(id);

        if (!taskOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        Task task = new Task();
        task.setId(id);
        task.setTitle(request.getTitle() != null ? request.getTitle() : taskOptional.get().getTitle());
        task.setDescription(
                request.getDescription() != null ? request.getDescription() : taskOptional.get().getDescription());
        task.setUserId(request.getUserId() != null ? request.getUserId() : taskOptional.get().getUserId());

        taskService.saveTask(task);

        return ResponseEntity.ok().body(task);
    }

}