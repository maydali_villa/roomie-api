package mx.roomie.resource;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import mx.roomie.model.Token;
import mx.roomie.model.User;
import mx.roomie.request.UserRequest;
import mx.roomie.service.UserService;

@RestController
public class UserResource {

  @Autowired
  private UserService userService;

  @PostMapping("/signup")
  public ResponseEntity<User> signup(@RequestBody UserRequest request) {
    User user = new User();
    user.setUsername(request.getUsername());
    user.setProfilePicture(request.getProfilePicture());
    user.setFirstName(request.getFirstName());
    user.setLastName(request.getLastName());
    user.setEmail(request.getEmail());
    user.setPassword(new BCryptPasswordEncoder().encode(request.getPassword()));

    userService.saveUser(user);

    return ResponseEntity.status(HttpStatus.CREATED).body(user);
  }

  @PostMapping("/login")
  public ResponseEntity<Token> login(@RequestBody UserRequest request) {
    User user = userService.getUserByEmail(request.getEmail());

    if (user == null) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    if ( !(new BCryptPasswordEncoder().matches(request.getPassword(), user.getPassword())) ) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }

    String token = userService.generateToken(request.getEmail());
    Token tokenResponse = new Token();
    tokenResponse.setToken(token);

    return ResponseEntity.ok().body(tokenResponse);
  }

  @GetMapping("/users")
  public ResponseEntity<List<User>> getUsers() {
    List<User> users = userService.getUsers();
    ResponseEntity<List<User>> response = ResponseEntity.ok().body(users);
    return response;
  }

  @GetMapping("/user")
  public ResponseEntity<User> getUser(@RequestParam("id") Integer id) {
    Optional<User> userOptional = userService.getUserById(id);

    if (!userOptional.isPresent()) {
      return ResponseEntity.notFound().build();
    }
    
    User user = userOptional.get();
    return ResponseEntity.ok().body(user);
    
  }

  @DeleteMapping("/user")
  public ResponseEntity<User> deleteUser(@RequestParam("id") Integer id) {
    userService.deleteUser(id);
    return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
  }

  @PutMapping("/user")
  public ResponseEntity<User> updateUser(@RequestParam("id") Integer id, @RequestBody UserRequest request) {
    Optional<User> userOptional = userService.getUserById(id);

    if (!userOptional.isPresent()) {
      return ResponseEntity.notFound().build();
    }

    User user = new User();
    user.setId(id);
    user.setUsername(request.getUsername() != null ? request.getUsername() : userOptional.get().getUsername());
    user.setFirstName(request.getFirstName() != null ? request.getFirstName() : userOptional.get().getFirstName());
    user.setLastName(request.getLastName() != null ? request.getLastName() : userOptional.get().getLastName());
    user.setEmail(request.getEmail() != null ? request.getEmail() : userOptional.get().getEmail());
    user.setProfilePicture(request.getProfilePicture() != null ? request.getProfilePicture() : userOptional.get().getProfilePicture());
    user.setPassword(
      request.getPassword() != null 
      ? new BCryptPasswordEncoder().encode(request.getPassword())
      : userOptional.get().getPassword()
    );

    userService.saveUser(user);

    return ResponseEntity.ok().body(user);
  }

}