package mx.roomie.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Jwts;
import mx.roomie.config.SecurityConstants;
import mx.roomie.model.User;
import mx.roomie.repository.UserRepository;

@Service
public class UserService {
  @Autowired
  private UserRepository userRepository;

  public List<User> getUsers() {
    List<User> users = new LinkedList<>();
    userRepository.findAll().iterator().forEachRemaining(users::add);
    return users;
  }

  public Optional<User> getUserById(Integer id) {
    return userRepository.findById(id);
  }

  public User getUserByEmail(String email) {
    return userRepository.findByEmail(email);
  }

  public void saveUser(User user) {
    userRepository.save(user);
  }

  public void deleteUser(Integer id) {
    userRepository.deleteById(id);
  }

  public String generateToken(String email) {
    String token = Jwts.builder()
      .setSubject(email)
      .signWith(SecurityConstants.SECRET_KEY)
      .compact();

    return token;
  }
}