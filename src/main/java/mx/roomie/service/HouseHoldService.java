package mx.roomie.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.roomie.model.HouseHold;
import mx.roomie.repository.HouseHoldRepository;

@Service
public class HouseHoldService {
    @Autowired
    private HouseHoldRepository houseHoldRepository;

    public List<HouseHold> getHouseHolds() {
        List<HouseHold> houseHolds = new LinkedList<>();
        houseHoldRepository.findAll().iterator().forEachRemaining(houseHolds::add);
        return houseHolds;
    }

    public Optional<HouseHold> getHouseHoldById(Integer id) {
        return houseHoldRepository.findById(id);
    }

    public void saveHouseHold(HouseHold houseHold) {
        houseHoldRepository.save(houseHold);
    }

    public void deleteHouseHold(Integer id) {
        houseHoldRepository.deleteById(id);
    }
}