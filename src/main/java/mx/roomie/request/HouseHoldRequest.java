package mx.roomie.request;

import java.util.List;

public class HouseHoldRequest {
    private String street;
    private String colony;
    private String country;
    private String city;
    private String address_line1;
    private String address_line2;
    /*
     * @Transient private List<User> users;
     * 
     * 
     * @Transient private List<Image> images;
     * 
     * @Transient private List<Rule> rules;
     */

    public String getAddress_line1() {
        return address_line1;
    }

    public String getAddress_line2() {
        return address_line2;
    }

    public String getCity() {
        return city;
    }

    public String getColony() {
        return colony;
    }

    public String getCountry() {
        return country;
    }

    public String getStreet() {
        return street;
    }

    public void setAddress_line1(String address_line1) {
        this.address_line1 = address_line1;
    }

    public void setAddress_line2(String address_line2) {
        this.address_line2 = address_line2;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setColony(String colony) {
        this.colony = colony;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    /*
     * public List<Image> getImages() { return images; }
     * 
     * public List<Rule> getRules() { return rules; }
     * 
     * 
     * public List<User> getUsers() { return users; }
     * 
     * /* public void setImages(List<Image> images) { this.images = images; }
     * 
     * public void setRules(List<Rule> rules) { this.rules = rules; }
     * 
     * 
     * public void setUsers(List<User> users) { this.users = users; }
     */
}